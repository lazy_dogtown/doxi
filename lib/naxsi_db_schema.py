

from mongoengine import *
from datetime import datetime as dt
from socket import getfqdn

import time

class Event(Document):
    peer_ip      = StringField(max_length=20, required=True)
    host         = StringField(max_length=1024, required=True)
    url          = StringField(required=True)
    rule_id      = IntField( required=True)
    content      = StringField(max_length=1024, required=True)
    zone         = StringField(max_length=1024, required=True)
    var_name     = StringField(required=True)
    sensor_id    = StringField(max_length=1024, default=getfqdn())
    remarks      = StringField(default="")
    tstamp       = IntField( required=True)
    dstamp       = DateTimeField(default=dt.now)
    
    meta = {
        'allow_inheritance': True,
        'collection': 'events', 
        'indexes': ['rule_id', 'peer_ip', 'url', 'tstamp', 
                    ('rule_id', 'peer_ip'), ('rule_id', 'url'), ('peer_ip', 'url'),
                     ('rule_id', 'tstamp'),  ('peer_ip', 'tstamp'), ('url', 'tstamp'),   ],
        'ordering': '-tstamp',
        

    }

    @queryset_manager
    def objects(doc_cls, queryset):
        # This may actually also be done by defining a default ordering for
        # the document, but this illustrates the use of manager methods
        return queryset.order_by('-tstamp')

class Temp(DynamicDocument):
    tag          = StringField(max_length=255, required=True)
    tdata        = DictField()
    meta = {
        'allow_inheritance' : True,
        'collection'        : 'doxi_temp', 
        'indexes'           : ['tag']

    }


class Stats(DynamicDocument):
    tag          = StringField(max_length=255, required=True)

    meta = {
        'allow_inheritance' : True,
        'collection'        : 'doxi_stats', 
        'indexes'           : ['tag',]

    }


class Messages(DynamicDocument):
    from time import time
    tag          = StringField(max_length=255, required=True)
    tstamp       = IntField( required=True, default=int(time()))
    
    meta = {
        'allow_inheritance' : True,
        'collection'        : 'doxi_messages', 
        'indexes'           : ['tag', 'tstamp']

    }


class UserData(DynamicDocument):
    from time import time
    tag          = StringField(max_length=255, required=True)
    tstamp       = IntField( required=True, default=int(time()))
    
    meta = {
        'allow_inheritance' : True,
        'collection'        : 'doxi_user_data', 
        'indexes'           : ['tag', 'tstamp']

    }

class Agent(DynamicDocument):
    from doxi_lib import random_string
    tag             = StringField(max_length=255, required=True)
    next_run        = IntField( required=True, default=int(time.time()))
    user_id         = StringField(max_length=255, required=True)
    agent_id        = StringField(required=True, default = random_string(16), unique = True)
    data            = DictField()
                      # a[name], a[a_id], a[timestep], a[srch] 
    status          = IntField( required=True, default=3)
                      # 0 -> manual / 1 -> active
    timestamp       = IntField( required=True, default=int(time.time()))
    
    meta = {
        'allow_inheritance' : True,
        'collection'        : 'doxi_agents', 
        'indexes'           : ['tag', 'next_run','user_id', 'agent_id']

    }

class AgentLog(DynamicDocument):


    tag             = StringField(max_length=255, required=True)
    timestamp       = IntField( required=True, default=int(time.time()))
    agent_id        = StringField(required=True)
    msg             = StringField(required=True)
    data            = DictField()
    status          = IntField(default=4)
                      # 0 -> OK :: 1 -> warn :: 2 ->crit  :: 4 - info, :: 5 - unknown 
    
    
    meta = {
        'allow_inheritance' : True,
        'collection'        : 'doxi_logs', 
        'indexes'           : ['tag', 'timestamp', 'agent_id', 'status']

    }


